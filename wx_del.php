<?php

$max = mt_rand(40,80);

for($i=0; $i < $max; $i++) {
    // 点击用户列表
    $rx = mt_rand(150, 600) + mt_rand(1000,2000)/1000;
    $ry = mt_rand(220,250) + mt_rand(1000,2000)/1000;
    $cmd = "adb shell input tap {$rx} {$ry}";
    $rand_time = mt_rand(500000,1500000);
    usleep($rand_time);
    system($cmd);
    
    // 点击"..."
    $rx = mt_rand(655, 666) + mt_rand(100,200)/1000;
    $ry = mt_rand(90,100) + mt_rand(100,200)/1000;
    $cmd = "adb shell input tap {$rx} {$ry}";
    // $cmd = "adb shell input tap 650 100";
    $rand_time = mt_rand(1000000,1800000);
    usleep($rand_time);
    system($cmd);
    // 上滑
    $px = mt_rand(400,600) + mt_rand(1000,2000)/1000;
    $py = mt_rand(1000,1100) + mt_rand(1000,2000)/1000;
    $ux = $px + mt_rand(-10,10);
    $uy = $py + mt_rand(-220,-160);
    $rand_time = mt_rand(200, 600);
    $cmd = "adb shell input swipe {$px} {$py} {$ux} {$uy} {$rand_time}";
    $rand_time = mt_rand(500000,1000000);
    usleep($rand_time);
    system($cmd);
    // 点击删除
    $rx = mt_rand(100, 650) + mt_rand(1000,2000)/1000;
    $ry = mt_rand(1270,1310) + mt_rand(1000,2000)/1000;
    $cmd = "adb shell input tap {$rx} {$ry}";
    $rand_time = mt_rand(500000,1000000);
    usleep($rand_time);
    system($cmd);
    // 点击删除确认
    $rx = mt_rand(450, 570) + mt_rand(1000,2000)/1000;
    $ry = mt_rand(820,860) + mt_rand(1000,2000)/1000;
    $cmd = "adb shell input tap {$rx} {$ry}";
    $rand_time = mt_rand(500000,1200000);
    usleep($rand_time);
    system($cmd);
    $rand_time = mt_rand(1000000,1800000);
    usleep($rand_time);
}


