## 依赖

 - php-cli (>=5.6)
 - adb 调试工具
 - android 手机

## 使用方式

 1. 手机进入设置 > 开发者选项，打开 USB 调试->模拟触控
 2. 手机连接电脑，安装 adb 相关驱动，检查 `adb devices` 命令是否能显示设备 ID; 链接地址：[https://adbshell.com/downloads](https://adbshell.com/downloads)
 	1. 补充adbdriver下载地址[http://dl.adbdriver.com/upload/adbdriver.zip](http://dl.adbdriver.com/upload/adbdriver.zip)
 3. 根据自己的手机,进行x,y坐标的微调;可以开启开发者模式->指针位置, 来确定x,y坐标位置的确切范围
 4. 调整好以后电脑运行 `php wx_del.php`